﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

class Program()
{
   static void Main()
   {
      Console.Write("Enter full file path: ");
      string path = Console.ReadLine();

      string[] fileData = File.ReadAllLines(path);
      List<Employee> list = new List<Employee>();
      for (int i = 0; i < fileData.Length; i++)
      {
         string[] line = fileData[i].Split(',');
         var employee = new Employee(line[0], line[1], double.Parse(line[2]));

         list.Add(employee);
      }

      Console.Write("Enter salary: ");
      double salaryInput = double.Parse(Console.ReadLine());

      var emails = list.Where(obj => obj.salary > salaryInput).OrderBy(obj => obj.email).Select(obj => obj.email);
      Console.WriteLine($"Email of people whose salary is more than ${salaryInput}");

      foreach (var email in emails)
      {
         Console.WriteLine(email);
      }

      var sum = list.Where(obj => obj.name[0] == 'M').Sum(obj => obj.salary);
      Console.WriteLine($"Sum of salary of people whose name stars with the letter 'M': {sum.ToString("F2")}");
   }
}