using System;

class Employee
{
   public string name;
   public string email;
   public double salary;

   public Employee(string Name, string Email, double Salary)
   {
      name = Name;
      email = Email;
      salary = Salary;
   }
}